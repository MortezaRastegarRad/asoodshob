from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    phone_number = models.CharField(max_length=10, null=False, blank=False)
    address = models.CharField(max_length=350, null=False, blank=False)

    def __str__(self):
        return '{}-{}'.format(self.username, self.phone_number)
