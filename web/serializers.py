from rest_framework import serializers
from .models import Tutorial, Product, Image, Category, OrderDetail, Order, Video


class TutorialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tutorial
        fields = ('id',
                  'title',
                  'description',
                  'published')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'id',
            'name'
        ]


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = [
            'id', 'url', 'product', 'image'
        ]
        extra_kwargs = {
            'image': {'write_only': True},
        }


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = [
            'id', 'url', 'product', 'video'
        ]
        extra_kwargs = {
            'video': {'write_only': True},
        }


class ProductSerializer(serializers.ModelSerializer):
    image_urls = ImageSerializer(many=True, source='images', read_only=True)
    video_urls = VideoSerializer(many=True, source='videos', read_only=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'name',
            'quantity',
            'category',
            'price',
            'created_at',
            'updated_at',
            'image_urls',
            'video_urls'
        ]


class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = [
            'id',
            'user',
            'product',
            'quantity',
            'status'
        ]


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            'id',
            'order_detail'
        ]
