import os

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from account.models import User
from .consts import get_choices, OrderStatus, IMAGE_BUCKET_NAME
from .arvan_cload import get_s3_session_instance


class Category(models.Model):
    name = models.CharField(max_length=10, null=False, blank=False)

    def __str__(self):
        return '{}'.format(self.name)


class Product(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    price = models.DecimalField(max_digits=16, decimal_places=4)
    quantity = models.IntegerField(default=0)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {} {}'.format(self.category, self.name, self.id)


def get_image_filename(instance, filename):
    return 'static/{}'.format(filename)


class Image(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='static', null=True)
    url = models.CharField(max_length=400, blank=True, null=True)

    def __str__(self):
        return self.url


@receiver(post_save, sender=Image, dispatch_uid="update_stock_count")
def update_image_url(sender, instance, **kwargs):
    if str(instance.url).startswith('/static/') or not instance.url:
        path = instance.image.path
        name = path.split('/')[-1]
        url = get_s3_session_instance().upload_file('static/' + name, IMAGE_BUCKET_NAME, name)
        instance.url = url
        instance.save()
        delete_file_from_local(path)


def get_video_filename(instance, filename):
    return 'media/{}'.format(filename)


class Video(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='videos')
    video = models.FileField(upload_to=get_video_filename, null=True)
    url = models.CharField(max_length=400, blank=True, null=True)

    def __str__(self):
        return self.url


@receiver(post_save, sender=Video, dispatch_uid="update_stock_count")
def update_image_url(sender, instance, **kwargs):
    if str(instance.url).startswith('/media/') or not instance.url:
        path = instance.video.path
        name = path.split('/')[-1]
        url = get_s3_session_instance().upload_file('media/' + name, IMAGE_BUCKET_NAME, name)
        instance.url = url
        instance.save()
        delete_file_from_local(path)


def delete_file_from_local(path):
    if os.path.exists(path):
        os.remove(path)


class OrderDetail(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    status = models.CharField(choices=get_choices(OrderStatus), max_length=10, default='pending')

    def __str__(self):
        return '{} - {} - {}'.format(self.id, self.user, self.product)


class Order(models.Model):
    order_detail = models.ManyToManyField(OrderDetail)

    def __str__(self):
        return '{} - {}'.format(self.id, self.order_detail.all())


class Tutorial(models.Model):
    title = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200, blank=False, default='')
    published = models.BooleanField(default=False)
