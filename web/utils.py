from kavenegar import *

from web.consts import KAVENEGAR_API_KEY

try:
    api = KavenegarAPI(KAVENEGAR_API_KEY)
    params = {
        'receptor': '09221227620',
        'template': 'myverification',
        'token': '852596',
        'type': 'sms',  # sms vs call
    }
    response = api.verify_lookup(params)
    print(response)
except APIException as e:
    print(e)
except HTTPException as e:
    print(e)
