from enum import Enum

CATEGORY_CHOICES = (
    ("clothing", "Clothing"),
    ("car", "Car"),
    ("toy", "Toy"),
    ("mobile", "Mobile")
)
CLOAD_SHOP_URL = 'https://s3.ir-thr-at1.arvanstorage.com'
CLOAD_SHOP_SECRET_KEY = '58174842751150186c1a298895b2f0a124daecd1abd3a43763099a32633d10b1'
CLOAD_SHOP_API_KEY = 'c4472e7a-9e61-4b68-a5b7-25c137521cdc'
IMAGE_BUCKET_NAME = 'asoodshop'

KAVENEGAR_API_KEY = '4E6951686274475374334F5630746347394B3851326751562F313438436E4C4D57453031785831566C636F3D'


class OrderStatus(Enum):
    pending = 'Pending'
    ready = 'Ready'
    complete = 'Complete'
    sending = 'Sending'


def get_choices(items):
    choices = []
    for item in items:
        choices.append((item.name, item.value))

    return tuple(choices)
