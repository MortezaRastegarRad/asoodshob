import json
from decimal import Decimal

from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse

# initialize the APIClient app
from web.models import Category, Product

client = Client()


class CreateCategoryTest(TestCase):
    """ Test module for GET all puppies API """

    def setUp(self):
        self.valid_payload = {
            'name': 'Muffin'
        }
        self.invalid_payload = {
            'age': ''
        }

    def test_create_category(self):
        response = client.post(
            reverse('category'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = client.post(
            reverse('category'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GetAllPuppiesTest(TestCase):
    """ Test module for GET all puppies API """

    def setUp(self):
        category = Category.objects.create(name='phone')
        category.save()
        Product.objects.create(name='galaxy s', price='12300', quantity=10, category=category).save()

    def test_get_all_puppies(self):
        product = Product.objects.get(name='galaxy s')
        self.assertEqual(str(product.price), '12300.0000')
