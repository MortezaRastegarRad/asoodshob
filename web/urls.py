from django.urls import re_path, path
from . import views

urlpatterns = [
    re_path(r'^api/tutorials$', views.tutorial_list),
    re_path(r'^api/tutorials/(?P<pk>[0-9]+)$', views.tutorial_detail),
    re_path(r'^api/tutorials/published$', views.tutorial_list_published),
    path('image/', views.ImageListApiView.as_view()),
    path('video/', views.VideoListApiView.as_view()),
    path('category/', views.CategoryListApiView.as_view(), name='category'),
    path('product/', views.ProductListApiView.as_view()),
    path('order_detail/', views.OrderDetailListApiView.as_view()),
    path('order/', views.OrderListApiView.as_view())
]
