import boto3
from .consts import CLOAD_SHOP_API_KEY, CLOAD_SHOP_SECRET_KEY, CLOAD_SHOP_URL


class ArvanCload:
    clients = {}

    def __init__(self, access_key, secret_key, url):
        self.access_key = access_key
        self.secret_key = secret_key
        self.url = url
        client = ArvanCload.clients.get(access_key + secret_key, None)
        if client:
            self.client = client
        else:
            session = boto3.session.Session()
            client = session.client(
                service_name='s3',
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_key,
                endpoint_url=url,
            )
            self.client = client
            ArvanCload.clients[access_key + secret_key] = self

    def get_bucket_keys(self, bucket):
        """Get a list of keys in an S3 bucket."""
        keys = []
        resp = self.client.list_objects_v2(Bucket=bucket)
        print(resp)
        for obj in resp['Contents']:
            keys.append(obj['Key'])
        return keys

    def upload_file(self, filename, bucket_name, name_in_bucket):
        self.client.upload_file(filename, bucket_name, name_in_bucket)
        return self.client.generate_presigned_url(
            'get_object',
            Params={
                'Bucket': bucket_name,
                'Key': name_in_bucket
            }
        )

    def delete_file(self, bucket_name, key):
        self.client.delete_object(Bucket=bucket_name, Key=key)


def get_s3_session_instance():
    instance = ArvanCload.clients.get(CLOAD_SHOP_API_KEY + CLOAD_SHOP_SECRET_KEY, None)
    if instance:
        return instance
    instance = ArvanCload(CLOAD_SHOP_API_KEY, CLOAD_SHOP_SECRET_KEY, CLOAD_SHOP_URL)
    return instance
