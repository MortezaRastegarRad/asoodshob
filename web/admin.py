from django.contrib import admin
from .models import Tutorial, Order, Product, Category, OrderDetail, Image, Video

admin.site.register(Tutorial)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderDetail)
admin.site.register(Image)
admin.site.register(Video)
